﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HttpFileTraining.Models
{
    public class Processor
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = "";

        [JsonPropertyName("brand")]
        public string Brand { get; set; } = "";

        [JsonPropertyName("year")]
        public string Year { get; set; } = "";
    }
}
