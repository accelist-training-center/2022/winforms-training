using HttpFileTraining.Models;
using System.Net.Http.Headers;
using System.Text.Json;

namespace HttpFileTraining
{
    public partial class Form1 : Form
    {
        private HttpClient HttpClient;

        public Form1()
        {
            InitializeComponent();
            this.HttpClient = new HttpClient();
        }

        private async void FetchAPIButton_Click(object sender, EventArgs e)
        {
            var response = await HttpClient.GetAsync("https://swapi.dev/api/people/");
            if (response == null)
            {
                MessageBox.Show("Response is empty!", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStreamAsync();

            PeopleResult? resultData = await JsonSerializer.DeserializeAsync<PeopleResult>(result);

            if (resultData == null)
            {
                MessageBox.Show("JSON conversion failed.", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var source = new BindingSource();
            source.DataSource = resultData.Results;

            NameList.DataSource = source;
            NameList.DisplayMember = "Name";
        }

        private async void PostAPIButton_Click(object sender, EventArgs e)
        {
            var endpoint = "https://reqres.in/api/users";

            var userRequest = new UserRequest
            {
                Name = "Sophie Neuenmuller",
                Job = "leader",
            };

            Stream userRequestJsonStream = new MemoryStream();

            await JsonSerializer.SerializeAsync<UserRequest>(userRequestJsonStream, userRequest);

            var content = new StreamContent(userRequestJsonStream);
            // content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            content.Headers.Add("Content-Type", "application/json");

            var response = await HttpClient.PostAsync(endpoint, content);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStreamAsync();

            var userResponse = await JsonSerializer.DeserializeAsync<UserResponse>(result);

            if (userResponse == null)
            {
                MessageBox.Show("Return result is null!", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show($"Data successfully submitted!\nID: {userResponse.Id}\nCreated At: {userResponse.CreatedAt}");
        }

        private async void ReadFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                var fileStream = new FileStream("C:\\Users\\Damillora\\Documents\\file.txt", FileMode.Open);
                StreamReader reader = new StreamReader(fileStream);
                var fileContents = await reader.ReadToEndAsync();

                APIResultTextBox.Text = fileContents;

                reader.Close();
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File does not exist!", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void WriteFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                var fileStream = new FileStream("C:\\Users\\Damillora\\Documents\\file.txt", FileMode.Create);
                StreamWriter writer = new StreamWriter(fileStream);

                await writer.WriteLineAsync(APIResultTextBox.Text);

                writer.Close();
            }
            catch (Exception)
            {

            }
        }

        private async void FetchLocalDataButton_Click(object sender, EventArgs e)
        {
            try
            {
                var fileStream = new FileStream("C:\\Users\\Damillora\\Downloads\\test-file.json", FileMode.Open);

                var result = await JsonSerializer.DeserializeAsync<List<Processor>>(fileStream);

                if (result == null)
                {
                    MessageBox.Show("Cannot parse JSON", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                var source = new BindingSource();
                source.DataSource = result;

                ProcessorList.DataSource = source;
                ProcessorList.DisplayMember = "Name";

                fileStream.Close();
            }
            catch (FileNotFoundException)
            {

            }
        }

        private async void WriteAPIDataButton_Click(object sender, EventArgs e)
        {
            var source = NameList.DataSource as BindingSource;
            var list = source?.DataSource as List<People>;

            var tempStream = new MemoryStream();
            await JsonSerializer.SerializeAsync<List<People>>(tempStream, list);

            var fileStream = new FileStream("C:\\Users\\Damillora\\Documents\\star-wars.json", FileMode.Create);
            tempStream.Seek(0, SeekOrigin.Begin);
            await tempStream.CopyToAsync(fileStream);

            fileStream.Close();
        }

        private void OpenImageButton_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "All images|*.jpg;*.png";
            dialog.Multiselect = false;

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var fileName = dialog.FileName;

                var fileStream = new FileStream(fileName, FileMode.Open);

                var image = Image.FromStream(fileStream);

                FilePictureBox.Image = image;
                FilePictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        private void CheckDateButton_Click(object sender, EventArgs e)
        {
            var today = DateTime.Today.Date;
            var selectedDate = DatePicker.Value.Date;

            var difference = selectedDate - today;

            if (difference.TotalDays >= 0)
            {
                MessageBox.Show($"The date is {difference.TotalDays} days after today", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"The date is {Math.Abs(difference.TotalDays)} days before today", "Http File Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void NameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = NameList.SelectedItem as People;

            if (item == null)
            {
                return;
            }

            NameData.Text = item.Name;
            BirthYearData.Text = item.BirthYear;
        }
    }
}