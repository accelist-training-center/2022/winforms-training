﻿namespace HttpFileTraining
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FetchAPIButton = new System.Windows.Forms.Button();
            this.APIResultTextBox = new System.Windows.Forms.TextBox();
            this.NameList = new System.Windows.Forms.ListBox();
            this.PostAPIButton = new System.Windows.Forms.Button();
            this.WriteFileButton = new System.Windows.Forms.Button();
            this.ReadFileButton = new System.Windows.Forms.Button();
            this.FetchLocalDataButton = new System.Windows.Forms.Button();
            this.ProcessorList = new System.Windows.Forms.ListBox();
            this.WriteAPIDataButton = new System.Windows.Forms.Button();
            this.OpenImageButton = new System.Windows.Forms.Button();
            this.FilePictureBox = new System.Windows.Forms.PictureBox();
            this.DatePicker = new System.Windows.Forms.DateTimePicker();
            this.CheckDateButton = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.BirthYearLabel = new System.Windows.Forms.Label();
            this.NameData = new System.Windows.Forms.TextBox();
            this.BirthYearData = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.FilePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // FetchAPIButton
            // 
            this.FetchAPIButton.Location = new System.Drawing.Point(12, 12);
            this.FetchAPIButton.Name = "FetchAPIButton";
            this.FetchAPIButton.Size = new System.Drawing.Size(100, 23);
            this.FetchAPIButton.TabIndex = 0;
            this.FetchAPIButton.Text = "Fetch API";
            this.FetchAPIButton.UseVisualStyleBackColor = true;
            this.FetchAPIButton.Click += new System.EventHandler(this.FetchAPIButton_Click);
            // 
            // APIResultTextBox
            // 
            this.APIResultTextBox.Location = new System.Drawing.Point(12, 41);
            this.APIResultTextBox.Multiline = true;
            this.APIResultTextBox.Name = "APIResultTextBox";
            this.APIResultTextBox.Size = new System.Drawing.Size(391, 142);
            this.APIResultTextBox.TabIndex = 1;
            // 
            // NameList
            // 
            this.NameList.FormattingEnabled = true;
            this.NameList.ItemHeight = 15;
            this.NameList.Location = new System.Drawing.Point(417, 44);
            this.NameList.Name = "NameList";
            this.NameList.Size = new System.Drawing.Size(162, 139);
            this.NameList.TabIndex = 2;
            this.NameList.SelectedIndexChanged += new System.EventHandler(this.NameList_SelectedIndexChanged);
            // 
            // PostAPIButton
            // 
            this.PostAPIButton.Location = new System.Drawing.Point(12, 189);
            this.PostAPIButton.Name = "PostAPIButton";
            this.PostAPIButton.Size = new System.Drawing.Size(96, 23);
            this.PostAPIButton.TabIndex = 3;
            this.PostAPIButton.Text = "Post API";
            this.PostAPIButton.UseVisualStyleBackColor = true;
            this.PostAPIButton.Click += new System.EventHandler(this.PostAPIButton_Click);
            // 
            // WriteFileButton
            // 
            this.WriteFileButton.Location = new System.Drawing.Point(114, 218);
            this.WriteFileButton.Name = "WriteFileButton";
            this.WriteFileButton.Size = new System.Drawing.Size(96, 23);
            this.WriteFileButton.TabIndex = 4;
            this.WriteFileButton.Text = "Write File";
            this.WriteFileButton.UseVisualStyleBackColor = true;
            this.WriteFileButton.Click += new System.EventHandler(this.WriteFileButton_Click);
            // 
            // ReadFileButton
            // 
            this.ReadFileButton.Location = new System.Drawing.Point(12, 218);
            this.ReadFileButton.Name = "ReadFileButton";
            this.ReadFileButton.Size = new System.Drawing.Size(96, 23);
            this.ReadFileButton.TabIndex = 5;
            this.ReadFileButton.Text = "Read File";
            this.ReadFileButton.UseVisualStyleBackColor = true;
            this.ReadFileButton.Click += new System.EventHandler(this.ReadFileButton_Click);
            // 
            // FetchLocalDataButton
            // 
            this.FetchLocalDataButton.Location = new System.Drawing.Point(12, 247);
            this.FetchLocalDataButton.Name = "FetchLocalDataButton";
            this.FetchLocalDataButton.Size = new System.Drawing.Size(198, 23);
            this.FetchLocalDataButton.TabIndex = 6;
            this.FetchLocalDataButton.Text = "Fetch Local Data";
            this.FetchLocalDataButton.UseVisualStyleBackColor = true;
            this.FetchLocalDataButton.Click += new System.EventHandler(this.FetchLocalDataButton_Click);
            // 
            // ProcessorList
            // 
            this.ProcessorList.FormattingEnabled = true;
            this.ProcessorList.ItemHeight = 15;
            this.ProcessorList.Location = new System.Drawing.Point(417, 189);
            this.ProcessorList.Name = "ProcessorList";
            this.ProcessorList.Size = new System.Drawing.Size(162, 139);
            this.ProcessorList.TabIndex = 7;
            // 
            // WriteAPIDataButton
            // 
            this.WriteAPIDataButton.Location = new System.Drawing.Point(12, 276);
            this.WriteAPIDataButton.Name = "WriteAPIDataButton";
            this.WriteAPIDataButton.Size = new System.Drawing.Size(198, 23);
            this.WriteAPIDataButton.TabIndex = 8;
            this.WriteAPIDataButton.Text = "Write API Data";
            this.WriteAPIDataButton.UseVisualStyleBackColor = true;
            this.WriteAPIDataButton.Click += new System.EventHandler(this.WriteAPIDataButton_Click);
            // 
            // OpenImageButton
            // 
            this.OpenImageButton.Location = new System.Drawing.Point(12, 305);
            this.OpenImageButton.Name = "OpenImageButton";
            this.OpenImageButton.Size = new System.Drawing.Size(198, 23);
            this.OpenImageButton.TabIndex = 9;
            this.OpenImageButton.Text = "Open File";
            this.OpenImageButton.UseVisualStyleBackColor = true;
            this.OpenImageButton.Click += new System.EventHandler(this.OpenImageButton_Click);
            // 
            // FilePictureBox
            // 
            this.FilePictureBox.Location = new System.Drawing.Point(12, 334);
            this.FilePictureBox.Name = "FilePictureBox";
            this.FilePictureBox.Size = new System.Drawing.Size(198, 104);
            this.FilePictureBox.TabIndex = 10;
            this.FilePictureBox.TabStop = false;
            // 
            // DatePicker
            // 
            this.DatePicker.Location = new System.Drawing.Point(216, 334);
            this.DatePicker.Name = "DatePicker";
            this.DatePicker.Size = new System.Drawing.Size(187, 23);
            this.DatePicker.TabIndex = 11;
            // 
            // CheckDateButton
            // 
            this.CheckDateButton.Location = new System.Drawing.Point(216, 363);
            this.CheckDateButton.Name = "CheckDateButton";
            this.CheckDateButton.Size = new System.Drawing.Size(187, 23);
            this.CheckDateButton.TabIndex = 12;
            this.CheckDateButton.Text = "Check Date";
            this.CheckDateButton.UseVisualStyleBackColor = true;
            this.CheckDateButton.Click += new System.EventHandler(this.CheckDateButton_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(594, 44);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(42, 15);
            this.NameLabel.TabIndex = 13;
            this.NameLabel.Text = "Name:";
            // 
            // BirthYearLabel
            // 
            this.BirthYearLabel.AutoSize = true;
            this.BirthYearLabel.Location = new System.Drawing.Point(594, 68);
            this.BirthYearLabel.Name = "BirthYearLabel";
            this.BirthYearLabel.Size = new System.Drawing.Size(60, 15);
            this.BirthYearLabel.TabIndex = 14;
            this.BirthYearLabel.Text = "Birth Year:";
            // 
            // NameData
            // 
            this.NameData.Location = new System.Drawing.Point(668, 41);
            this.NameData.Name = "NameData";
            this.NameData.ReadOnly = true;
            this.NameData.Size = new System.Drawing.Size(100, 23);
            this.NameData.TabIndex = 15;
            // 
            // BirthYearData
            // 
            this.BirthYearData.Location = new System.Drawing.Point(668, 65);
            this.BirthYearData.Name = "BirthYearData";
            this.BirthYearData.ReadOnly = true;
            this.BirthYearData.Size = new System.Drawing.Size(100, 23);
            this.BirthYearData.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BirthYearData);
            this.Controls.Add(this.NameData);
            this.Controls.Add(this.BirthYearLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.CheckDateButton);
            this.Controls.Add(this.DatePicker);
            this.Controls.Add(this.FilePictureBox);
            this.Controls.Add(this.OpenImageButton);
            this.Controls.Add(this.WriteAPIDataButton);
            this.Controls.Add(this.ProcessorList);
            this.Controls.Add(this.FetchLocalDataButton);
            this.Controls.Add(this.ReadFileButton);
            this.Controls.Add(this.WriteFileButton);
            this.Controls.Add(this.PostAPIButton);
            this.Controls.Add(this.NameList);
            this.Controls.Add(this.APIResultTextBox);
            this.Controls.Add(this.FetchAPIButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.FilePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button FetchAPIButton;
        private TextBox APIResultTextBox;
        private ListBox NameList;
        private Button PostAPIButton;
        private Button WriteFileButton;
        private Button ReadFileButton;
        private Button FetchLocalDataButton;
        private ListBox ProcessorList;
        private Button WriteAPIDataButton;
        private Button OpenImageButton;
        private PictureBox FilePictureBox;
        private DateTimePicker DatePicker;
        private Button CheckDateButton;
        private Label NameLabel;
        private Label BirthYearLabel;
        private TextBox NameData;
        private TextBox BirthYearData;
    }
}