﻿namespace DownloadTask
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SourceTextBox = new System.Windows.Forms.TextBox();
            this.TargetTextBox = new System.Windows.Forms.TextBox();
            this.SourceURLLabel = new System.Windows.Forms.Label();
            this.TargetDestinationLabel = new System.Windows.Forms.Label();
            this.DownloadButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SourceTextBox
            // 
            this.SourceTextBox.Location = new System.Drawing.Point(12, 32);
            this.SourceTextBox.Name = "SourceTextBox";
            this.SourceTextBox.Size = new System.Drawing.Size(288, 23);
            this.SourceTextBox.TabIndex = 0;
            // 
            // TargetTextBox
            // 
            this.TargetTextBox.Location = new System.Drawing.Point(12, 76);
            this.TargetTextBox.Name = "TargetTextBox";
            this.TargetTextBox.Size = new System.Drawing.Size(288, 23);
            this.TargetTextBox.TabIndex = 1;
            // 
            // SourceURLLabel
            // 
            this.SourceURLLabel.AutoSize = true;
            this.SourceURLLabel.Location = new System.Drawing.Point(12, 9);
            this.SourceURLLabel.Name = "SourceURLLabel";
            this.SourceURLLabel.Size = new System.Drawing.Size(67, 15);
            this.SourceURLLabel.TabIndex = 2;
            this.SourceURLLabel.Text = "Source URL";
            // 
            // TargetDestinationLabel
            // 
            this.TargetDestinationLabel.AutoSize = true;
            this.TargetDestinationLabel.Location = new System.Drawing.Point(12, 58);
            this.TargetDestinationLabel.Name = "TargetDestinationLabel";
            this.TargetDestinationLabel.Size = new System.Drawing.Size(97, 15);
            this.TargetDestinationLabel.TabIndex = 3;
            this.TargetDestinationLabel.Text = "Target Local Path";
            // 
            // DownloadButton
            // 
            this.DownloadButton.Location = new System.Drawing.Point(12, 105);
            this.DownloadButton.Name = "DownloadButton";
            this.DownloadButton.Size = new System.Drawing.Size(288, 23);
            this.DownloadButton.TabIndex = 4;
            this.DownloadButton.Text = "Download";
            this.DownloadButton.UseVisualStyleBackColor = true;
            this.DownloadButton.Click += new System.EventHandler(this.DownloadButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 208);
            this.Controls.Add(this.DownloadButton);
            this.Controls.Add(this.TargetDestinationLabel);
            this.Controls.Add(this.SourceURLLabel);
            this.Controls.Add(this.TargetTextBox);
            this.Controls.Add(this.SourceTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox SourceTextBox;
        private TextBox TargetTextBox;
        private Label SourceURLLabel;
        private Label TargetDestinationLabel;
        private Button DownloadButton;
    }
}