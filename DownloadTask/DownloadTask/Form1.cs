namespace DownloadTask
{
    public partial class Form1 : Form
    {
        private HttpClient HttpClient;

        public Form1()
        {
            InitializeComponent();
            HttpClient = new HttpClient();
        }

        private async void DownloadButton_Click(object sender, EventArgs e)
        {
            var sourceUrl = SourceTextBox.Text;
            var targetPath = TargetTextBox.Text;

            var response = await HttpClient.GetAsync(sourceUrl);
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStreamAsync();

            try
            {
                var fileStream = new FileStream(targetPath, FileMode.Create);
                await result.CopyToAsync(fileStream);

                fileStream.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("An error has occured", "Downloader", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}