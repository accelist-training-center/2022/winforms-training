﻿namespace WinFormsTraining
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SmallButton = new System.Windows.Forms.Button();
            this.BigButton = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.EnableCheckBox = new System.Windows.Forms.CheckBox();
            this.OptionOneRadio = new System.Windows.Forms.RadioButton();
            this.OptionTwoRadio = new System.Windows.Forms.RadioButton();
            this.CheckButton = new System.Windows.Forms.Button();
            this.ProgressUpDown = new System.Windows.Forms.NumericUpDown();
            this.ControlProgressBar = new System.Windows.Forms.ProgressBar();
            this.TypeComboBox = new System.Windows.Forms.ComboBox();
            this.AddDataButton = new System.Windows.Forms.Button();
            this.NowPlayingButton = new System.Windows.Forms.Button();
            this.FirstRadioGroupPanel = new System.Windows.Forms.Panel();
            this.SecondRadioFirstGroup = new System.Windows.Forms.RadioButton();
            this.FirstRadioFirstGroup = new System.Windows.Forms.RadioButton();
            this.SecondRadioGroupPanel = new System.Windows.Forms.Panel();
            this.SecondRadioSecondGroup = new System.Windows.Forms.RadioButton();
            this.FirstRadioSecondGroup = new System.Windows.Forms.RadioButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.NameList = new System.Windows.Forms.ListBox();
            this.DataForm = new System.Windows.Forms.TableLayoutPanel();
            this.CharacterNameLabel = new System.Windows.Forms.Label();
            this.CharacterSignLabel = new System.Windows.Forms.Label();
            this.CharacterPlaceLabel = new System.Windows.Forms.Label();
            this.CharacterNounLabel = new System.Windows.Forms.Label();
            this.CharacterNameTextBox = new System.Windows.Forms.TextBox();
            this.CharacterSignTextBox = new System.Windows.Forms.TextBox();
            this.CharacterPlaceTextBox = new System.Windows.Forms.TextBox();
            this.CharacterNounTextBox = new System.Windows.Forms.TextBox();
            this.CharacterSubmitButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressUpDown)).BeginInit();
            this.FirstRadioGroupPanel.SuspendLayout();
            this.SecondRadioGroupPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.DataForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // SmallButton
            // 
            this.SmallButton.Location = new System.Drawing.Point(162, 12);
            this.SmallButton.Name = "SmallButton";
            this.SmallButton.Size = new System.Drawing.Size(75, 23);
            this.SmallButton.TabIndex = 0;
            this.SmallButton.Text = "Submit";
            this.SmallButton.UseVisualStyleBackColor = true;
            this.SmallButton.Click += new System.EventHandler(this.SmallButton_Click);
            // 
            // BigButton
            // 
            this.BigButton.Location = new System.Drawing.Point(427, 40);
            this.BigButton.Name = "BigButton";
            this.BigButton.Size = new System.Drawing.Size(278, 124);
            this.BigButton.TabIndex = 1;
            this.BigButton.Text = "Start";
            this.BigButton.UseVisualStyleBackColor = true;
            this.BigButton.Click += new System.EventHandler(this.BigButton_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(12, 16);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(42, 15);
            this.NameLabel.TabIndex = 2;
            this.NameLabel.Text = "Name:";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(56, 13);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(100, 23);
            this.NameTextBox.TabIndex = 3;
            // 
            // EnableCheckBox
            // 
            this.EnableCheckBox.AutoSize = true;
            this.EnableCheckBox.Location = new System.Drawing.Point(427, 15);
            this.EnableCheckBox.Name = "EnableCheckBox";
            this.EnableCheckBox.Size = new System.Drawing.Size(100, 19);
            this.EnableCheckBox.TabIndex = 4;
            this.EnableCheckBox.Text = "Enable Button";
            this.EnableCheckBox.UseVisualStyleBackColor = true;
            this.EnableCheckBox.CheckedChanged += new System.EventHandler(this.EnableCheckBox_CheckedChanged);
            // 
            // OptionOneRadio
            // 
            this.OptionOneRadio.AutoSize = true;
            this.OptionOneRadio.Location = new System.Drawing.Point(14, 42);
            this.OptionOneRadio.Name = "OptionOneRadio";
            this.OptionOneRadio.Size = new System.Drawing.Size(71, 19);
            this.OptionOneRadio.TabIndex = 5;
            this.OptionOneRadio.TabStop = true;
            this.OptionOneRadio.Text = "Option 1";
            this.OptionOneRadio.UseVisualStyleBackColor = true;
            // 
            // OptionTwoRadio
            // 
            this.OptionTwoRadio.AutoSize = true;
            this.OptionTwoRadio.Location = new System.Drawing.Point(114, 42);
            this.OptionTwoRadio.Name = "OptionTwoRadio";
            this.OptionTwoRadio.Size = new System.Drawing.Size(71, 19);
            this.OptionTwoRadio.TabIndex = 6;
            this.OptionTwoRadio.TabStop = true;
            this.OptionTwoRadio.Text = "Option 2";
            this.OptionTwoRadio.UseVisualStyleBackColor = true;
            // 
            // CheckButton
            // 
            this.CheckButton.Location = new System.Drawing.Point(191, 40);
            this.CheckButton.Name = "CheckButton";
            this.CheckButton.Size = new System.Drawing.Size(75, 23);
            this.CheckButton.TabIndex = 7;
            this.CheckButton.Text = "Check";
            this.CheckButton.UseVisualStyleBackColor = true;
            this.CheckButton.Click += new System.EventHandler(this.CheckButton_Click);
            // 
            // ProgressUpDown
            // 
            this.ProgressUpDown.Location = new System.Drawing.Point(56, 67);
            this.ProgressUpDown.Name = "ProgressUpDown";
            this.ProgressUpDown.Size = new System.Drawing.Size(100, 23);
            this.ProgressUpDown.TabIndex = 8;
            this.ProgressUpDown.ValueChanged += new System.EventHandler(this.ProgressUpDown_ValueChanged);
            // 
            // ControlProgressBar
            // 
            this.ControlProgressBar.Location = new System.Drawing.Point(14, 100);
            this.ControlProgressBar.Name = "ControlProgressBar";
            this.ControlProgressBar.Size = new System.Drawing.Size(252, 29);
            this.ControlProgressBar.TabIndex = 9;
            // 
            // TypeComboBox
            // 
            this.TypeComboBox.FormattingEnabled = true;
            this.TypeComboBox.Location = new System.Drawing.Point(147, 146);
            this.TypeComboBox.Name = "TypeComboBox";
            this.TypeComboBox.Size = new System.Drawing.Size(119, 23);
            this.TypeComboBox.TabIndex = 11;
            // 
            // AddDataButton
            // 
            this.AddDataButton.Location = new System.Drawing.Point(147, 175);
            this.AddDataButton.Name = "AddDataButton";
            this.AddDataButton.Size = new System.Drawing.Size(119, 23);
            this.AddDataButton.TabIndex = 12;
            this.AddDataButton.Text = "Fetch Data";
            this.AddDataButton.UseVisualStyleBackColor = true;
            this.AddDataButton.Click += new System.EventHandler(this.AddDataButton_Click);
            // 
            // NowPlayingButton
            // 
            this.NowPlayingButton.Location = new System.Drawing.Point(147, 204);
            this.NowPlayingButton.Name = "NowPlayingButton";
            this.NowPlayingButton.Size = new System.Drawing.Size(119, 23);
            this.NowPlayingButton.TabIndex = 13;
            this.NowPlayingButton.Text = "Now Playing";
            this.NowPlayingButton.UseVisualStyleBackColor = true;
            this.NowPlayingButton.Click += new System.EventHandler(this.NowPlayingButton_Click);
            // 
            // FirstRadioGroupPanel
            // 
            this.FirstRadioGroupPanel.Controls.Add(this.SecondRadioFirstGroup);
            this.FirstRadioGroupPanel.Controls.Add(this.FirstRadioFirstGroup);
            this.FirstRadioGroupPanel.Location = new System.Drawing.Point(19, 250);
            this.FirstRadioGroupPanel.Name = "FirstRadioGroupPanel";
            this.FirstRadioGroupPanel.Size = new System.Drawing.Size(200, 27);
            this.FirstRadioGroupPanel.TabIndex = 14;
            // 
            // SecondRadioFirstGroup
            // 
            this.SecondRadioFirstGroup.AutoSize = true;
            this.SecondRadioFirstGroup.Location = new System.Drawing.Point(103, 3);
            this.SecondRadioFirstGroup.Name = "SecondRadioFirstGroup";
            this.SecondRadioFirstGroup.Size = new System.Drawing.Size(94, 19);
            this.SecondRadioFirstGroup.TabIndex = 1;
            this.SecondRadioFirstGroup.TabStop = true;
            this.SecondRadioFirstGroup.Text = "radioButton2";
            this.SecondRadioFirstGroup.UseVisualStyleBackColor = true;
            // 
            // FirstRadioFirstGroup
            // 
            this.FirstRadioFirstGroup.AutoSize = true;
            this.FirstRadioFirstGroup.Location = new System.Drawing.Point(3, 3);
            this.FirstRadioFirstGroup.Name = "FirstRadioFirstGroup";
            this.FirstRadioFirstGroup.Size = new System.Drawing.Size(94, 19);
            this.FirstRadioFirstGroup.TabIndex = 0;
            this.FirstRadioFirstGroup.TabStop = true;
            this.FirstRadioFirstGroup.Text = "radioButton1";
            this.FirstRadioFirstGroup.UseVisualStyleBackColor = true;
            // 
            // SecondRadioGroupPanel
            // 
            this.SecondRadioGroupPanel.Controls.Add(this.SecondRadioSecondGroup);
            this.SecondRadioGroupPanel.Controls.Add(this.FirstRadioSecondGroup);
            this.SecondRadioGroupPanel.Location = new System.Drawing.Point(19, 280);
            this.SecondRadioGroupPanel.Name = "SecondRadioGroupPanel";
            this.SecondRadioGroupPanel.Size = new System.Drawing.Size(200, 25);
            this.SecondRadioGroupPanel.TabIndex = 15;
            // 
            // SecondRadioSecondGroup
            // 
            this.SecondRadioSecondGroup.AutoSize = true;
            this.SecondRadioSecondGroup.Location = new System.Drawing.Point(103, 3);
            this.SecondRadioSecondGroup.Name = "SecondRadioSecondGroup";
            this.SecondRadioSecondGroup.Size = new System.Drawing.Size(94, 19);
            this.SecondRadioSecondGroup.TabIndex = 1;
            this.SecondRadioSecondGroup.TabStop = true;
            this.SecondRadioSecondGroup.Text = "radioButton4";
            this.SecondRadioSecondGroup.UseVisualStyleBackColor = true;
            // 
            // FirstRadioSecondGroup
            // 
            this.FirstRadioSecondGroup.AutoSize = true;
            this.FirstRadioSecondGroup.Location = new System.Drawing.Point(3, 3);
            this.FirstRadioSecondGroup.Name = "FirstRadioSecondGroup";
            this.FirstRadioSecondGroup.Size = new System.Drawing.Size(94, 19);
            this.FirstRadioSecondGroup.TabIndex = 0;
            this.FirstRadioSecondGroup.TabStop = true;
            this.FirstRadioSecondGroup.Text = "radioButton3";
            this.FirstRadioSecondGroup.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Controls.Add(this.button4);
            this.flowLayoutPanel1.Controls.Add(this.button5);
            this.flowLayoutPanel1.Controls.Add(this.button6);
            this.flowLayoutPanel1.Controls.Add(this.button7);
            this.flowLayoutPanel1.Controls.Add(this.button8);
            this.flowLayoutPanel1.Controls.Add(this.button9);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 361);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(800, 89);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(165, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(246, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(327, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 17;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(408, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 18;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(489, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 19;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(570, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 20;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(651, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 21;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // NameList
            // 
            this.NameList.FormattingEnabled = true;
            this.NameList.ItemHeight = 15;
            this.NameList.Location = new System.Drawing.Point(18, 146);
            this.NameList.Name = "NameList";
            this.NameList.Size = new System.Drawing.Size(120, 94);
            this.NameList.TabIndex = 10;
            // 
            // DataForm
            // 
            this.DataForm.ColumnCount = 2;
            this.DataForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DataForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DataForm.Controls.Add(this.CharacterNameLabel, 0, 0);
            this.DataForm.Controls.Add(this.CharacterSignLabel, 0, 1);
            this.DataForm.Controls.Add(this.CharacterPlaceLabel, 0, 2);
            this.DataForm.Controls.Add(this.CharacterNounLabel, 0, 3);
            this.DataForm.Controls.Add(this.CharacterNameTextBox, 1, 0);
            this.DataForm.Controls.Add(this.CharacterSignTextBox, 1, 1);
            this.DataForm.Controls.Add(this.CharacterPlaceTextBox, 1, 2);
            this.DataForm.Controls.Add(this.CharacterNounTextBox, 1, 3);
            this.DataForm.Controls.Add(this.CharacterSubmitButton, 0, 4);
            this.DataForm.Controls.Add(this.ClearButton, 1, 4);
            this.DataForm.Location = new System.Drawing.Point(427, 175);
            this.DataForm.Name = "DataForm";
            this.DataForm.RowCount = 5;
            this.DataForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.DataForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.DataForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.DataForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.DataForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.DataForm.Size = new System.Drawing.Size(278, 166);
            this.DataForm.TabIndex = 17;
            // 
            // CharacterNameLabel
            // 
            this.CharacterNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharacterNameLabel.AutoSize = true;
            this.CharacterNameLabel.Location = new System.Drawing.Point(3, 9);
            this.CharacterNameLabel.Name = "CharacterNameLabel";
            this.CharacterNameLabel.Size = new System.Drawing.Size(39, 15);
            this.CharacterNameLabel.TabIndex = 0;
            this.CharacterNameLabel.Text = "Name";
            // 
            // CharacterSignLabel
            // 
            this.CharacterSignLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharacterSignLabel.AutoSize = true;
            this.CharacterSignLabel.Location = new System.Drawing.Point(3, 42);
            this.CharacterSignLabel.Name = "CharacterSignLabel";
            this.CharacterSignLabel.Size = new System.Drawing.Size(30, 15);
            this.CharacterSignLabel.TabIndex = 1;
            this.CharacterSignLabel.Text = "Sign";
            // 
            // CharacterPlaceLabel
            // 
            this.CharacterPlaceLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharacterPlaceLabel.AutoSize = true;
            this.CharacterPlaceLabel.Location = new System.Drawing.Point(3, 75);
            this.CharacterPlaceLabel.Name = "CharacterPlaceLabel";
            this.CharacterPlaceLabel.Size = new System.Drawing.Size(35, 15);
            this.CharacterPlaceLabel.TabIndex = 2;
            this.CharacterPlaceLabel.Text = "Place";
            // 
            // CharacterNounLabel
            // 
            this.CharacterNounLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CharacterNounLabel.AutoSize = true;
            this.CharacterNounLabel.Location = new System.Drawing.Point(3, 108);
            this.CharacterNounLabel.Name = "CharacterNounLabel";
            this.CharacterNounLabel.Size = new System.Drawing.Size(37, 15);
            this.CharacterNounLabel.TabIndex = 3;
            this.CharacterNounLabel.Text = "Noun";
            // 
            // CharacterNameTextBox
            // 
            this.CharacterNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CharacterNameTextBox.Location = new System.Drawing.Point(142, 5);
            this.CharacterNameTextBox.Name = "CharacterNameTextBox";
            this.CharacterNameTextBox.Size = new System.Drawing.Size(133, 23);
            this.CharacterNameTextBox.TabIndex = 4;
            // 
            // CharacterSignTextBox
            // 
            this.CharacterSignTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CharacterSignTextBox.Location = new System.Drawing.Point(142, 38);
            this.CharacterSignTextBox.Name = "CharacterSignTextBox";
            this.CharacterSignTextBox.Size = new System.Drawing.Size(133, 23);
            this.CharacterSignTextBox.TabIndex = 5;
            // 
            // CharacterPlaceTextBox
            // 
            this.CharacterPlaceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CharacterPlaceTextBox.Location = new System.Drawing.Point(142, 71);
            this.CharacterPlaceTextBox.Name = "CharacterPlaceTextBox";
            this.CharacterPlaceTextBox.Size = new System.Drawing.Size(133, 23);
            this.CharacterPlaceTextBox.TabIndex = 6;
            // 
            // CharacterNounTextBox
            // 
            this.CharacterNounTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CharacterNounTextBox.Location = new System.Drawing.Point(142, 104);
            this.CharacterNounTextBox.Name = "CharacterNounTextBox";
            this.CharacterNounTextBox.Size = new System.Drawing.Size(133, 23);
            this.CharacterNounTextBox.TabIndex = 7;
            // 
            // CharacterSubmitButton
            // 
            this.CharacterSubmitButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CharacterSubmitButton.Location = new System.Drawing.Point(3, 137);
            this.CharacterSubmitButton.Name = "CharacterSubmitButton";
            this.CharacterSubmitButton.Size = new System.Drawing.Size(133, 23);
            this.CharacterSubmitButton.TabIndex = 8;
            this.CharacterSubmitButton.Text = "Submit";
            this.CharacterSubmitButton.UseVisualStyleBackColor = true;
            this.CharacterSubmitButton.Click += new System.EventHandler(this.CharacterSubmitButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ClearButton.Location = new System.Drawing.Point(142, 137);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(133, 23);
            this.ClearButton.TabIndex = 9;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.DataForm);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.SecondRadioGroupPanel);
            this.Controls.Add(this.FirstRadioGroupPanel);
            this.Controls.Add(this.NowPlayingButton);
            this.Controls.Add(this.AddDataButton);
            this.Controls.Add(this.TypeComboBox);
            this.Controls.Add(this.NameList);
            this.Controls.Add(this.ControlProgressBar);
            this.Controls.Add(this.ProgressUpDown);
            this.Controls.Add(this.CheckButton);
            this.Controls.Add(this.OptionTwoRadio);
            this.Controls.Add(this.OptionOneRadio);
            this.Controls.Add(this.EnableCheckBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.BigButton);
            this.Controls.Add(this.SmallButton);
            this.Name = "Form1";
            this.Text = "Win Forms Training";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProgressUpDown)).EndInit();
            this.FirstRadioGroupPanel.ResumeLayout(false);
            this.FirstRadioGroupPanel.PerformLayout();
            this.SecondRadioGroupPanel.ResumeLayout(false);
            this.SecondRadioGroupPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.DataForm.ResumeLayout(false);
            this.DataForm.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button SmallButton;
        private Button BigButton;
        private Label NameLabel;
        private TextBox NameTextBox;
        private CheckBox EnableCheckBox;
        private RadioButton OptionOneRadio;
        private RadioButton OptionTwoRadio;
        private Button CheckButton;
        private NumericUpDown ProgressUpDown;
        private ProgressBar ControlProgressBar;
        private ComboBox TypeComboBox;
        private Button AddDataButton;
        private Button NowPlayingButton;
        private Panel FirstRadioGroupPanel;
        private RadioButton SecondRadioFirstGroup;
        private RadioButton FirstRadioFirstGroup;
        private Panel SecondRadioGroupPanel;
        private RadioButton SecondRadioSecondGroup;
        private RadioButton FirstRadioSecondGroup;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private ListBox NameList;
        private TableLayoutPanel DataForm;
        private Label CharacterNameLabel;
        private Label CharacterSignLabel;
        private Label CharacterPlaceLabel;
        private Label CharacterNounLabel;
        private TextBox CharacterNameTextBox;
        private TextBox CharacterSignTextBox;
        private TextBox CharacterPlaceTextBox;
        private TextBox CharacterNounTextBox;
        private Button CharacterSubmitButton;
        private Button ClearButton;
    }
}