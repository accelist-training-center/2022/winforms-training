using WinFormsTraining.Models;

namespace WinFormsTraining
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SmallButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Hello, {NameTextBox.Text}", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void EnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BigButton.Enabled = EnableCheckBox.Checked;
        }

        private void BigButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button enabled!", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CheckButton_Click(object sender, EventArgs e)
        {
            if (OptionOneRadio.Checked == true)
            {
                MessageBox.Show("You chose option 1!", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (OptionTwoRadio.Checked == true)
            {
                MessageBox.Show("You chose option 2!", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ProgressUpDown_ValueChanged(object sender, EventArgs e)
        {
            ControlProgressBar.Value = ((int)ProgressUpDown.Value);
        }

        private void AddDataButton_Click(object sender, EventArgs e)
        {
            var nameList = new List<string>() {
                "Sophie Neuenmuller",
                "Plachta",
                "Ramizel Erlenmeyer",
            };

            var bindingSource = new BindingSource();
            bindingSource.DataSource = nameList;

            NameList.DataSource = bindingSource;

            var typeList = new List<GenreType>()
            {
                new GenreType
                {
                    Name = "Role-Playing Game",
                    Shortname = "RPG",
                },
                new GenreType
                {
                    Name = "First-Person Shooter",
                    Shortname = "FPS",
                },
                new GenreType
                {
                    Name = "Real-Time Strategy",
                    Shortname = "RTS",
                }
            };

            var typeDataSource = new BindingSource();
            typeDataSource.DataSource = typeList;

            TypeComboBox.DataSource = typeDataSource;
            TypeComboBox.DisplayMember = "Shortname";
        }

        private void NowPlayingButton_Click(object sender, EventArgs e)
        {
            var name = NameList.SelectedItem as string;
            var type = TypeComboBox.SelectedItem as GenreType;

            MessageBox.Show($"{name} is playing {type.Name} / {type.Shortname}", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            CharacterNameTextBox.Text = "";
            CharacterSignTextBox.Text = "";
            CharacterPlaceTextBox.Text = "";
            CharacterNounTextBox.Text = "";
        }

        private void CharacterSubmitButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CharacterNameTextBox.Text) == true)
            {
                MessageBox.Show("Name must not be empty", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (CharacterNameTextBox.Text.Contains(' ') == false)
            {
                MessageBox.Show("Name must consists of two words", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(CharacterSignTextBox.Text) == true)
            {
                MessageBox.Show("Sign must not be empty", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(string.IsNullOrEmpty(CharacterPlaceTextBox.Text) == true)
            {
                MessageBox.Show("Place must not be empty", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(CharacterPlaceTextBox.Text.EndsWith("land")== false)
            {
                MessageBox.Show("Place must end with land", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(CharacterNounTextBox.Text) == true)
            {
                MessageBox.Show("Noun must not be empty", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show($"{CharacterNameTextBox.Text}, a {CharacterSignTextBox.Text}, found a {CharacterNounTextBox.Text} in {CharacterPlaceTextBox.Text}", "Windows Forms Training", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}