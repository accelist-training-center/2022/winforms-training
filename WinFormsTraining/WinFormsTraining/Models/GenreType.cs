﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsTraining.Models
{
    /// <summary>
    /// Model class for genre type
    /// </summary>
    public class GenreType
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the short name.
        /// </summary>
        public string Shortname { get; set; }
    }
}
